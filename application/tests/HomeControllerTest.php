<?php declare(strict_types=1);

namespace Mini\Tests;

use PHPUnit\Framework\TestCase;
use Mini\Model\Registry;

final class HomeControllerTest extends TestCase
{
    /**
     * @test
     */
    public function registryUser(): void
    {
        $this->assertTrue((new Registry())->registry('usertest@test.com', '1234qwer', 'Test User', 'CO', '598-246-152'), "registry"
        );
    }
}