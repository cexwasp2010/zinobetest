<?php

/**
 * Class Registrys
 * This is a demo Model class.
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Mini\Model;

use Mini\Core\Model;

use PHPAuth\Config as PHPAuthConfig;
use PHPAuth\Auth as PHPAuth;

class Registry extends Model
{
    /**
     * Get auth registry from database
     * @return boolean
     */
    public function getAuth()
    {
        $dbh = $this->db;
        $config = new PHPAuthConfig($dbh);
        $auth = new PHPAuth($dbh, $config); 
        return $auth;
    }

    /**
     * Get user registry from database
     * @return array $data
     */
    public function getUser()
    {
        $dbh = $this->db;
        $config = new PHPAuthConfig($dbh);
        $auth = new PHPAuth($dbh, $config); 
        return $auth->getCurrentUser();
    }

    /**
     * Set user data for registry in database
     * @return array boolean
     */
    public function registry($email, $password, $name, $country, $document)
    {
        $dbh = $this->db;
        $config = new PHPAuthConfig($dbh);
        $auth = new PHPAuth($dbh, $config); 
        $result = $auth->register($email, $password, $password, $params = ["document" => $document, "name" => $name, "country" => $country], null, null);
        return $result;
    }

    /**
     * Get a registry from database
     * @param string $email email of user login
     * @param string $password 
     * @return boolean if login is successful
     */
    public function login($email, $password)
    {
        $dbh = $this->db;
        $config = new PHPAuthConfig($dbh);
        $auth = new PHPAuth($dbh, $config); 
        $result = $auth->login($email, $password);
        return $result;
    }
 
    /**
     * Get a registry from database
     * @return boolean if logout successful
     */
    public function logout()
    {
        $dbh = $this->db;
        $config = new PHPAuthConfig($dbh);
        $auth = new PHPAuth($dbh, $config); 
        $result = $auth->logout($auth->getCurrentSessionHash());
        return $result;
    }
}
