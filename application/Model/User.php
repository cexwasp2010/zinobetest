<?php

/**
 * Class Users
 * This is a demo Model class.
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Mini\Model;

use Mini\Core\Model;

class User extends Model
{
    /**
     * Get all users from database
     */
    public function getAllUsers()
    {
        $sql = "SELECT id, name, email FROM phpauth_users";
        $query = $this->db->prepare($sql);
        $query->execute();

        // fetchAll() is the PDO method that gets all result rows, here in object-style because we defined this in
        // core/controller.php! If you prefer to get an associative array as the result, then do
        // $query->fetchAll(PDO::FETCH_ASSOC); or change core/controller.php's PDO options to
        // $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC ...
        return $query->fetchAll();
    }

    /**
     * Get a user from database
     * @param integer $name
     */
    public function getUser($name)
    {
        $sql = "SELECT id, name, email, document, country FROM phpauth_users WHERE name like '%".$name."%' or email like '%".$name."%'";
        $query = $this->db->prepare($sql);

        // useful for debugging: you can see the SQL behind above construction by using:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute();

        // fetch() is the PDO method that get exactly one result
        return ($query->rowcount() ? $query->fetchAll() : false);
    }
}
