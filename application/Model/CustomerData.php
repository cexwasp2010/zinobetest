<?php

/**
 * Class Country
 * This is a demo Model class.
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Mini\Model;

use Mini\Core\Model;

class CustomerData extends Model
{

    /**
     * Get all users from database
     */
    public function initRequest(string $document_user)
    {
        $sql = "SELECT * FROM externals_request WHERE document_user like '". $document_user ."';";
        $query = $this->db->prepare($sql);
        $query->execute();

        if ($query->rowcount() == 0) {
            $url = "http://www.mocky.io/v2/5d9f39263000005d005246ae?mocky-delay=10s";
            $data1 = json_decode(file_get_contents($url, true), true);
            $results = array_filter($data1['objects'], function($data) use ($document_user) {
                return $data['document'] == $document_user;
            });
            if (empty($results)){
                $url = "http://www.mocky.io/v2/5d9f38fd3000005b005246ac?mocky-delay=10s";
                $data2 = json_decode(file_get_contents($url, true), true);
                $results = array_filter($data2['objects'], function($data) use ($document_user) {
                    return $data['cedula'] == $document_user;
                });
                if (!empty($results)){
                    $sql = "INSERT INTO externals_request (`url_request`, `document_user`, `json_response`) VALUES ('" . $url. "', '" . $document_user . "', '". json_encode($results) . "'); ";
                    $query = $this->db->prepare($sql);
                    $query->execute();
                    echo "Se a encontrado información externa!";
                }
                if (empty($results)){
                    echo "No se encontró información externa!";
                }
            }else{
                $sql = "INSERT INTO externals_request (`url_request`, `document_user`, `json_response`) VALUES ('" . $url. "', '" . $document_user . "', '". json_encode($results) . "'); ";
                $query = $this->db->prepare($sql);
                $query->execute();    
                echo "Se a encontrado información externa!";
            }
        }else{
            echo "Se a encontrado información externa!";
            //@TODO: descomentar para mostrar los resultados de la base de datos var_dump($query->fetchAll());
        }
    }

}
