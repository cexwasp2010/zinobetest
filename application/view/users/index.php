<h2 class="navigation">
    <?php echo "Yuo are logged as " . $user_name?>
    <p><span id="loading"></span></p>
</h2>
<div class="container">
    <?php if (isset($message)): ?>
        <div class="container">
            <span class="invalid-feedback" role="alert">
                <strong><?php echo $message ?></strong>
            </span>
        </div>
    <?php endif; ?>
    <h1>Users</h1>
    <h2>In this page you can search any user by name or email</h2>
    <!-- add user form -->
    <div class="box">
        <h3>Search</h3>
        <form>
            <label>Name or email</label>
            <input type="text" name="name" onkeyup="searchUser(this.value)" value="<?php echo $name?>" />
            <!--input type="submit" name="submit_search_user" value="Search" /-->
        </form>
    </div>
    <!-- main content output -->
    <div class="box">
        <h3>List of users</h3>
        <div id="results">
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function (){
        document.getElementById("loading").innerHTML = "Loading data ...";
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("loading").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET", "<?php echo URL; ?>users/callcustomerdata?user_document=<?php echo $user_document;?>", true);
        xmlhttp.send();
    }
    function searchUser(str) {
        if (str.length == 0) {
            document.getElementById("results").innerHTML = "";
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("results").innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET", "<?php echo URL; ?>users/searchuser?name=" + str, true);
            xmlhttp.send();
        }
    }
</script>
