		<table>
            <thead style="background-color: #ddd; font-weight: bold;">
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Email</td>
                <td>Document</td>
                <td>Country</td>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($users) && $users !== false): ?>
	            <?php foreach ($users as $user) { ?>
	                <tr>
	                    <td><?php if (isset($user->id)) echo htmlspecialchars($user->id, ENT_QUOTES, 'UTF-8'); ?></td>
	                    <td><?php if (isset($user->name)) echo htmlspecialchars($user->name, ENT_QUOTES, 'UTF-8'); ?></td>
	                    <td><?php if (isset($user->email)) echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
	                    <td><?php if (isset($user->document)) echo htmlspecialchars($user->document, ENT_QUOTES, 'UTF-8'); ?></td>
	                    <td><?php if (isset($user->country)) echo htmlspecialchars($user->country, ENT_QUOTES, 'UTF-8'); ?></td>
	                </tr>
	            <?php } ?>
            <?php elseif (isset($users)): ?>
            	<h3 style="color:red;">No results found</h3>
            <?php endif; ?>
            </tbody>
        </table>