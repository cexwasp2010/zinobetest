<!-- 
Autor Cesar Córdoba
 -->

        <div id="registry" class="modal">
          
            <form class="modal-content animate" action="<?php echo URL; ?>home/comprobateform" method="POST">
                <?php if (isset($message)): ?>
                    <div class="container">
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo $message ?></strong>
                        </span>
                    </div>
                <?php endif; ?>
                <div class="container">
                    <label for="name"><b>Name</b></label>
                    <input id="name" type="text" placeholder="Input Name" class="form-control is-invalid" name="name" minlength="3" value="<?php echo $name?>" required autocomplete="name" autofocus>
                    <label for="email"><b>Email</b></label>
                    <input id="email" type="email" placeholder="Input email" class="form-control is-invalid" name="email" value="<?php echo $email?>" required autocomplete="email" autofocus>

                    <label for="document"><b>Document</b></label>
                    <input id="document" type="text" placeholder="Input Document" class="form-control is-invalid" name="document" value="<?php echo $document?>" required autocomplete="document" autofocus>

                    <label for="country"><b>Country</b></label>
                    <select id="country" name="country" value="<?php echo $country?>">
						<?php
						    foreach ($countries as $country){
						        echo '<option value="'.$country->code.'">' . $country->name . '</option>';
						    }
						?>
					</select>

                    <label for="password"><b>Password</b></label>
                    <input id="password" type="password" placeholder="Input password" class="form-control is-invalid" name="password" minlength="6" value="" required autocomplete="password" autofocus>

                    <button name="submit_registry_user" type="submit" value="Registry">Registrarse</button>
                </div>

                <div class="container" style="background-color:#f1f1f1">
                </div>
            </form>
        </div>