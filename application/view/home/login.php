<!-- 
Autor Cesar Córdoba
 -->
        <div id="login" class="modal">
          
            <form class="modal-content animate" action="<?php echo URL; ?>home/loginuser" method="POST">
               <?php if (isset($message)): ?>
                    <div class="container">
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo $message ?></strong>
                        </span>
                    </div>
                <?php endif; ?>
                 <div class="container">
                    <label for="email"><b>Email</b></label>
                    <input type="email" placeholder="Input your email" name="email" value="<?php echo $email ?>" required>
                    
                    <label for="password"><b>Password</b></label>
                    <input type="password" placeholder="Input your password" name="password" required>

                    <button name="login" type="submit">Login</button>
                </div>

                <div class="container" style="background-color:#f1f1f1">
                </div>
            </form>
        </div>