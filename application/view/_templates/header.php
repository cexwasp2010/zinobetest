<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Zinobetest</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo URL; ?>css/style.css" rel="stylesheet">
</head>
<body>
    <!-- navigation -->
    <div class="navigation">
        <?php if ($logged): ?>
            <a href="<?php echo URL; ?>users">users</a>
            <a href="<?php echo URL; ?>home/logout">Loguot</a>
        <?php else: ?>
            <a href="<?php echo URL; ?>home/registry">registry</a>
            <a href="<?php echo URL; ?>home/login">login</a>
        <?php endif; ?>
    </div>
