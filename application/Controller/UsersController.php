<?php

/**
 * Class UsersController
 *
 * If you want, you can use multiple Models or Controllers.
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Mini\Controller;

use Mini\Model\User;
use Mini\Model\Registry;
use Mini\Model\CustomerData;

class UsersController
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/users/index
     */
    public function index()
    {
        $logged = false;
        $Registry = new Registry();
        // do getAuth() in model/registry.php
        $auth = $Registry->getAuth();
        if (!$auth->isLogged()) {
            $Home = new HomeController();
            $Home->login();
            // require APP . 'view/home/registry.php';
            exit();
        }else{
            // Get fields to current user logged
            $user = $Registry->getUser();
            $user_name = $user["name"];
            $user_document = $user["document"];
            $name = "";
            $logged = true;
            // load views
            require APP . 'view/_templates/header.php';
            require APP . 'view/users/index.php';
            require APP . 'view/_templates/footer.php';
        }
        
    }

     /**
     * @GET
     * ACTION: callcustomerdata
     * This method handles what happens when you move to http://yourproject/users/callcustomerdata
     */
    function callCustomerData(){
        // Init service request to External Server
        $CustomerData = new CustomerData();
        $CustomerData->initRequest($_GET["user_document"]);
    }

     /**
     * ACTION: searchUser
     * This method handles what happens when you move to http://yourproject/users/searchuser
     */
    public function searchUser()
    {
        $logged = true;
        $name = $_GET["name"];
        $Registry = new Registry();
        $user = $Registry->getUser();
        $user_name = $user["name"];
        $user_document = $user["document"];
        // if we have an id of a user that should be found
        if (!empty($_GET["name"])) {
            // Instance new Model (User)
            $User = new User();
            // do getUser() in model/model.php
            $users = $User->getUser($_GET["name"]);
        }else{
            $users = false;
        }
        // load views. within the views we can echo out $user easily
        include(APP . 'view/users/searchuser.php');
//        require APP . 'view/_templates/header.php';
//        require APP . 'view/users/index.php';
//        require APP . 'view/_templates/footer.php';
    }

}
