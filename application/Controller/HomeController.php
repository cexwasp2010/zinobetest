<?php

/**
 * Class HomeController
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Mini\Controller;

use Mini\Model\Country;
use Mini\Model\Registry;

class HomeController
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index()
    {
        // load views
        $Registry = new Registry();
        // do getAuth() in model/registry.php for return instance of Auth
        $auth = $Registry->getAuth();
        if (!$auth->isLogged()) {
            $this->login();
        }else{
            $logged = true;
            $users = new UsersController();
            $users->index();
        }
    }

    /**
     * PAGE: registry
     * This method handles what happens when you move to http://yourproject/home/registry
     * The camelCase writing is just for better readability. The method name is case-insensitive.
     */
    public function registry()
    {
        $name = '';
        $email = '';
        $document = '';
        $logged = false;
        $country = '';
        // Instance new Model (Country)
        $Country = new Country();
        // do getUser() in model/model.php
        $countries = $Country->getAllCountries();
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/registry.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * PAGE: comprobateForm
     * This method handles what happens when you move to http://yourproject/home/comprobateform
     * The camelCase writing is just for better readability. The method name is case-insensitive.
     */
    public function comprobateForm()
    {
        $logged = false;
        $name = $_POST['name'];
        $email = $_POST['email'];
        $document = $_POST['document'];
        $country = $_POST['country'];
        $Registry = new Registry();
        // do getAuth() in model/registry.php for return instance of Auth
        $result = $Registry->registry($_POST['email'], $_POST['password'], $_POST['name'], $_POST['country'], $_POST['document']);
        $message = "";
        if ($result["error"]){
            // Instance new Model (Country)
            $Country = new Country();
            // do getUser() in model/model.php
            $countries = $Country->getAllCountries();
            
            $message = $result["message"];
            require APP . 'view/_templates/header.php';
            require APP . 'view/home/registry.php';
            require APP . 'view/_templates/footer.php';
        }else{
            $message = $result["message"];
            require APP . 'view/_templates/header.php';
            require APP . 'view/home/login.php';
            require APP . 'view/_templates/footer.php';
        }
    }

    /**
     * PAGE: loginUser
     * This method handles what happens when you move to http://yourproject/home/loginuser
     * The camelCase writing is just for better readability. The method name is case-insensitive.
     */
    public function loginUser()
    {
        $logged = false;
        $email = $_POST['email'];
        $Registry = new Registry();
        // do getAuth() in model/registry.php for login in Auth
        $result = $Registry->login($email, $_POST['password']);
        $message = "";
        if ($result["error"]){
            $message = $result["message"];
            // load views
            require APP . 'view/_templates/header.php';
            require APP . 'view/home/login.php';
            require APP . 'view/_templates/footer.php';
        }else{
            // load Users Views
            $Users = new UsersController();
            $Users->index($result["hash"]);
        }
    }

    /**
     * PAGE: logout
     * This method handles what happens when you move to http://yourproject/home/logout
     * The camelCase writing is just for better readability. The method name is case-insensitive.
     */
    public function logout()
    {
        $Registry = new Registry();
        // logout user if this is logged
        $result = $Registry->logout();
        $message = "";
        if ($result["error"]){
            $logged = true;
            $message = $result["message"];
            // load views
            require APP . 'view/_templates/header.php';
            require APP . 'view/users/index.php';
            require APP . 'view/_templates/footer.php';
        }else{
            $logged = false;
            $message = $result["message"];
            $this->login();
        }
    }

    /**
     * PAGE: login
     * This method handles what happens when you move to http://yourproject/home/login
     * The camelCase writing is just for better readability. The method name is case-insensitive.
     */
    public function login()
    {
       $Registry = new Registry();
        // do getAuth() in model/registry.php for return instance of Auth
        $auth = $Registry->getAuth();
        if (!$auth->isLogged()) {
            $email = '';
            $logged = false;
            // load views
            require APP . 'view/_templates/header.php';
            require APP . 'view/home/login.php';
            require APP . 'view/_templates/footer.php';
        }else{
            // load Users Views
            $Users = new UsersController();
            $Users->index();
        }
    }
}
