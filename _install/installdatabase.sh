#!/bin/sh

sudo mysql -u root -p < _install/create-database.sql;

composer install;

exit $RESULT
